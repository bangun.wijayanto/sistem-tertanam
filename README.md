<div align='center'>
    <h1><b>BELAJAR IOT </b></h1><br/><i>(Praktikum Sistem Tertanam)</i>
     <img src='https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2020/09/Getting-Started-Visual-Studio-VS-Code-PlatformIO-IDE-ESP32-ESP8266-NodeMCU-Windows-Mac-OS-X-Linux-Ubuntu.jpg?resize=1024%2C576&quality=100&strip=all&ssl=1'  />
  


![C](https://badgen.net/badge/C/[version]/blue?)
![C++](https://badgen.net/badge/C++/[version]/blue?)

</div>

---

## 💾 **ABOUT**

Suplemen matakuliah  ** _SISTEM TERTANAM_ 


<br />

---

## 🗒️ **Penggunaan**

### local installation:

1. Pastikan anda telah menginstall visual code studio, jika belum silahkan install dengan mengunduh pada alamat

```
https://code.visualstudio.com/

```

2. Install Plugin PlatformIO ikuti langkah langkah berikut

```
https://randomnerdtutorials.com/vs-code-platformio-ide-esp32-esp8266-arduino/

```

3. Lakukan praktikum dengan urutan

```
1. LED Project
2. Sensor Jarak
3. Servo Motor
4. LCD Hello Words
5. LED IOT

```


<br />

---

## 📎 **LICENSE**

MIT License

Copyright © [2023] [bangun wijayanto]

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

<br />

---

