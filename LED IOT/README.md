<div align='center'>
    <h1><b>[Kontrol LED dari Web (Praktikum 5)]</b></h1>
       <img src='https://gitlab.com/bangun.wijayanto/sistem-tertanam/-/raw/master/LED%20IOT/skema.png' />

   
    <p>Short description of the project.</p>


![C](https://badgen.net/badge/C/[version]/blue?)
![C++](https://badgen.net/badge/C++/[version]/blue?)

</div>

---

## 💾 **ABOUT**

Mencoba mengontrol led dari interface web _NodeMCU ESP 8266_ 
 `open source!`

<br />

---

## 🗒️ **Penggunaan**
Menghubungkan perangkat laptop/handphone pada jaringan yang sama dengan NodeMCU ESP8266, lalu mengakses alamat IP Addressnya di browser, kita akan diberikan halaman untuk mengontrol led tersebut
### local installation:

1. Pastikan wiring kabel sebagai berikut

```
+ LED 1 ---------------> D1
+ LED 2 ---------------> D2
+ LED 3 ---------------> D3
GND ---------------> GND

```


2. Setelah program berhasil kita upload, selanjutnya kita check keberhasilannya pada serial monitor.
3. Setelah berhasil, koneksikan laptop/smartphone kita pada hotspot yang dibuat oleh NodeMCU ESP8266 tersebut. Lalu masukan alamat IP ESP8266 di browser (IP yang ada di serial monitor)
4. Sekarang kita hanya perlu menekan tombol off dan on untuk menghidupkan dan mematikan led yang terhubung pada ESP8266. Dengan ini control led melalui webpage web server esp8266 sudah berhasil.


<br />

---

## 📎 **LICENSE**

MIT License

Copyright © [2023] [bangun wijayanto]

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

<br />

---

