#include<Arduino.h>
#include <Servo.h> // Library untuk Servo

Servo myservo; // Inisialisasi objek Servo

void setup() {
  myservo.attach(D1); // Pin Servo dihubungkan ke pin D1 pada NodeMCU
}

void loop() {
  for (int pos = 0; pos <= 180; pos += 1) { // Menggerakkan servo dari 0 derajat hingga 180 derajat
    myservo.write(pos); // Menggerakkan servo ke posisi tertentu
    delay(15); // Delay 15ms sebelum menggerakkan servo ke posisi selanjutnya
  }
  for (int pos = 180; pos >= 0; pos -= 1) { // Menggerakkan servo dari 180 derajat hingga 0 derajat
    myservo.write(pos); // Menggerakkan servo ke posisi tertentu
    delay(15); // Delay 15ms sebelum menggerakkan servo ke posisi selanjutnya
  }
}