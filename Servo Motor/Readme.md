<div align='center'>
    <h1><b>[Servo Motor]</b></h1>
     <img src='https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2018/05/servo-motor-wires.jpg?resize=750%2C443&quality=100&strip=all&ssl=1'  />
  


![C](https://badgen.net/badge/C/[version]/blue?)
![C++](https://badgen.net/badge/C++/[version]/blue?)

</div>

---

## 💾 **ABOUT**

Mencoba Menggerakan servo dengan ** _NodeMCU ESP 8266_ 
 `open source!`

<br />

---

## 🗒️ **Penggunaan**

### local installation:

1. Pertama-tama, kita mengimpor library Servo yang diperlukan untuk menggerakan servo.
Selanjutnya, kita inisialisasi objek myservo.

```
Pin Signal Servo ---------------> D1
```
2. Pada fungsi setup(), kita menghubungkan pin Servo ke pin D1 pada NodeMCU menggunakan method attach().
Pada fungsi loop(), kita menggerakkan servo dari 0 derajat hingga 180 derajat menggunakan loop for dengan increment 1. Setiap pergerakan servo dilakukan dengan method write(), dan terdapat delay 15ms sebelum menggerakkan servo ke posisi selanjutnya. Setelah mencapai posisi 180 derajat, kita menggerakkan servo kembali dari 180 derajat hingga 0 derajat menggunakan loop for dengan decrement 1.
3. Pastikan Anda telah menghubungkan servo dengan benar pada pin NodeMCU seperti yang didefinisikan pada kode di atas. Selain itu, pastikan juga bahwa servo yang digunakan mendukung pengaturan posisi dari 0 derajat hingga 180 derajat. Jika tidak, Anda perlu menyesuaikan nilai pada loop for sesuai dengan kemampuan servo yang digunakan.






<br />

---

## 📎 **LICENSE**

MIT License

Copyright © [2023] [bangun wijayanto]

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

<br />

---

