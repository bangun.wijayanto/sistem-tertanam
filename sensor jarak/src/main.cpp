#include <Arduino.h>

#include <NewPing.h> // Library untuk SR04

#define TRIGGER_PIN D1 // Pin trigger SR04 dihubungkan ke pin D1 pada NodeMCU
#define ECHO_PIN D2 // Pin echo SR04 dihubungkan ke pin D2 pada NodeMCU
#define MAX_DISTANCE 200 // Jarak maksimum yang dapat diukur (dalam cm)

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // Inisialisasi objek sonar

void setup() {
  Serial.begin(9600); // Inisialisasi komunikasi serial
}

void loop() {
  delay(50); // Delay 50 ms untuk mengurangi gangguan suara
  int distance = sonar.ping_cm(); // Membaca jarak dari sensor SR04 dalam cm
  Serial.print("Jarak: ");
  Serial.print(distance);
  Serial.println(" cm");
}