<div align='center'>
    <h1><b>[Sensor Jarak (Praktikum 2)]</b></h1>
     <img src='https://www.nn-digital.com/wp-content/uploads/2019/07/Sensor-HC-SR04-1.jpg'  />
  


![C](https://badgen.net/badge/C/[version]/blue?)
![C++](https://badgen.net/badge/C++/[version]/blue?)

</div>

---

## 💾 **ABOUT**

Mencoba mengukur jarak dengan ** _NodeMCU ESP 8266_ 
 `open source!`

<br />

---

## 🗒️ **Penggunaan**

### local installation:

1. Pertama-tama, kita mengimpor library NewPing yang diperlukan untuk membaca sensor jarak SR04.
Kemudian, kita mendefinisikan pin trigger dan pin echo pada NodeMCU yang dihubungkan ke SR04, serta jarak maksimum yang dapat diukur oleh SR04 (dalam cm).
```
Triger ---------------> D1
Echo ---------------> D2
```
2. Selanjutnya, kita inisialisasi objek sonar dengan menggunakan pin trigger, pin echo, dan jarak maksimum yang telah didefinisikan sebelumnya.
Pada fungsi setup(), kita inisialisasi komunikasi serial dengan baud rate 9600.
Pada fungsi loop(), kita membaca jarak dari sensor SR04 menggunakan method ping_cm() pada objek sonar. Hasil pembacaan jarak kemudian dicetak ke serial monitor menggunakan fungsi Serial.print() dan Serial.println(). 
3. Terdapat delay 50ms untuk mengurangi gangguan suara pada pembacaan sensor.
Pastikan Anda telah menghubungkan sensor SR04 dengan benar pada pin NodeMCU seperti yang didefinisikan pada kode di atas.






<br />

---

## 📎 **LICENSE**

MIT License

Copyright © [2023] [bangun wijayanto]

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

<br />

---

